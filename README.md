# Base32

A tiny package for decoding Base32 values.

## Usage

```
let data: Data = try Base32.decode("YKXVYXZI4OBYIKK7F7BK6===")
let string = String(data: data, encoding: .utf8)
// result: "¯\_(ツ)_/¯"
```
