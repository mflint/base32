import Foundation

public enum Base32 {
	enum Base32Error: Error {
		case unexpectedCharacter(Character)
	}

	static private let table = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
		.enumerated()
		.reduce(into: [Character : UInt8]()) { partialResult, element in
			partialResult[element.element] = UInt8(element.offset)
	}

	public static func decode(_ input: String) throws -> Data {
		var bytes = [UInt8]()

		// we add data to our accumulator 5 bits at a time, but
		// need to remove 8-bit values.
		// this accumulator is the current value
		var accumulator: UInt8 = 0

		// this keeps track of the number of bits of useful data
		// in the accumulator
		var collectedBits = 0

		for character in input {
			if character == "=" {
				break
			}

			// convert this character into a 5-bit value
			guard let fiveBits = Self.table[character] else {
				throw Base32Error.unexpectedCharacter(character)
			}

			// how many bits do we take from our new 5 bits?
			let bitsToTake = min(5, 8 - collectedBits)

			// shift these new bits into the accumulator
			// eg: if there are 6 useful bits already in the
			// accumulator, we'll need 2 more to make a full
			// 8-bit byte.
			// So we need to shift the accumulator left by
			// 2 bits, to make room:
			// 00111111 -> 11111100
			// Then we take the 2 most significant (leftmost)
			// bits from the new `fiveBits`, and add those to
			// the accumulator into the gap that we made
			accumulator = (accumulator << bitsToTake) + (fiveBits >> (5 - bitsToTake))

			// we've collected 5 more
			collectedBits += 5

			// if we have a full 8-bits of data...
			if collectedBits >= 8 {
				// then this is the next byte in our output
				bytes.append(accumulator)

				// the leftover (least-significant) bits in our
				// `fiveBits` value are our new accumulator value
				accumulator = (fiveBits & (0x1f >> bitsToTake))
				
				collectedBits -= 8
			}
		}

		return Data(bytes)
	}
}
