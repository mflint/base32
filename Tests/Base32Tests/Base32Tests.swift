import XCTest
@testable import Base32

extension Base32.Base32Error: Equatable {
	public static func == (lhs: Base32.Base32Error, rhs: Base32.Base32Error) -> Bool {
		switch (lhs, rhs) {
		case let (.unexpectedCharacter(c1), .unexpectedCharacter(c2)):
			return c1 == c2
		}
	}
}

final class Base32Tests: XCTestCase {
	private func assertBinary(_ input: String,
							  _ expected: [UInt8],
							  file: StaticString = #file,
							  line: UInt = #line) throws {
		let decoded = try Base32.decode(input).map { UInt8($0) }
		XCTAssertEqual(decoded, expected, file: file, line: line)
	}

	private func assertString(_ input: String,
							  _ expected: String,
							  file: StaticString = #file,
							  line: UInt = #line) throws {
		let decoded = try String(data: Base32.decode(input), encoding: .utf8)
		XCTAssertEqual(decoded, expected, file: file, line: line)
	}

	func testNothing() throws {
		try self.assertBinary("", [])
	}

	func testUnexpectedCharacter() throws {
		XCTAssertThrowsError(try Base32.decode("1")) { error in
			XCTAssertEqual(error as! Base32.Base32Error, Base32.Base32Error.unexpectedCharacter("1"))
		}
	}

	// the simplest encoded string without `=` padding
	func testFiveNulls() throws {
		// endless screaming
		try self.assertBinary("AAAAAAAA", [
			0b00000000,
			0b00000000,
			0b00000000,
			0b00000000,
			0b00000000
		])
	}

	// the simplest encoded string _with_ `=` padding
	// A     A
	// 00000 00000
	func testOneNull() throws {
		try self.assertBinary("AA======", [0b00000000])
	}

	// the first bit of the first 5-bit block
	// Q     A
	// 10000 00000
	func test0x80() throws {
		try self.assertBinary("QA======", [0b10000000])
	}

	// the last bit of the first 5-bit block
	// B     A
	// 00001 00000
	func test0x01() throws {
		try self.assertBinary("BA======", [0b00001000])
	}

	// the last bit of the first 5-bit block, plus the
	// the first bit of the second block
	// B     Q
	// 00001 10000
	func test0x0C() throws {
		try self.assertBinary("BQ======", [0b00001100])
	}

	// the last bit of the first 5-bit block, plus the
	// the first and fourth bit of the second block
	// B     S     A     A
	// 00001 10010 00000 00000
	//          ^ this fourth bit of the second block is the
	//            9th bit in the whole sequence, so should be
	//            used as the most significant bit in the
	//            second byte of output
	func test0x0C80() throws {
		try self.assertBinary("BSAA====", [0b00001100, 0b10000000])
	}

	func testEveryCharacter() throws {
		try self.assertBinary("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567",
							  [0x00, 0x44, 0x32, 0x14, 0xc7, 0x42, 0x54, 0xb6,
							   0x35, 0xcf, 0x84, 0x65, 0x3a, 0x56, 0xd7, 0xc6,
							   0x75, 0xbe, 0x77, 0xdf])
	}

	func testTheCrazyOnes() throws {
		try self.assertString("JBSXEZJHOMQHI3ZAORUGKIDDOJQXU6JAN5XGK4Y=",
							  "Here's to the crazy ones")
	}

	func testShrug() throws {
		try self.assertString("YKXVYXZI4OBYIKK7F7BK6===",
							  #"¯\_(ツ)_/¯"#)
	}
}
